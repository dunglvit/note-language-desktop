async function loadData(callback, textSearch) {
    const mysql = require('mysql');
    let connection = mysql.createConnection({
        host: 'localhost',
        port: '3306',
        user: 'root',
        password: 'root',
        database: 'note_language'
    });
    connection.connect(function () {
    });
    let query = 'SELECT * FROM `japanese` WHERE `kanji` LIKE ' + '"%' + textSearch + '%"' + ' OR `hiragana` LIKE ' + '"%' + textSearch + '%"' + ' OR `mean` LIKE ' + '"%' + textSearch + '%"'  + 'LIMIT 20';
    await connection.query(query, function (err, rows, fields) {
        if (err) {
            return;
        }
        callback(rows);
    });
    connection.end(function () {
    });
}

async function insertData(kanji, hiragana, mean) {
    const mysql = require('mysql');
    let connection = mysql.createConnection({
        host: 'localhost',
        port: '3306',
        user: 'root',
        password: 'root',
        database: 'note_language'
    });
    connection.connect(function () {
    });
    let query = 'INSERT INTO `japanese` (`kanji`, `hiragana`, `mean`) VALUES (' + '"' + kanji + '"' + ',' + '"' + hiragana + '"' + ',' + '"' + mean + '"' + ')';
    await connection.query(query, function (err, rows, fields) {
        if (err) {
            alert(err);
            return;
        }
    });
    connection.end(function () {
    });
}

async function updateData(number, kanji, hiragana, mean) {
    const mysql = require('mysql');
    let connection = mysql.createConnection({
        host: 'localhost',
        port: '3306',
        user: 'root',
        password: 'root',
        database: 'note_language'
    });
    connection.connect(function () {
    });
    let query = 'UPDATE `japanese` SET `kanji` = ' + '"' + kanji+ '"' + ', `hiragana` = ' + '"' + hiragana + '"' + ', `mean` = ' + '"' + mean + '"' + ' WHERE `id` = ' + number;
    await connection.query(query, function (err, rows, fields) {
        if (err) {
            alert(err);
            return;
        }
    });
    connection.end(function () {
    });
}

async function notification(){
    const notifier = require('node-notifier');
    const mysql = require('mysql');
    let connection = mysql.createConnection({
        host: 'localhost',
        port: '3306',
        user: 'root',
        password: 'root',
        database: 'note_language'
    });
    connection.connect(function () {
    });
    let query = 'SELECT * FROM `japanese` ORDER BY RAND() LIMIT 1';
    await connection.query(query, function (err, row, fields) {
        if (err) {
            return;
        }
        if(row.length > 0){
            notifier.notify({
                title: row[0].kanji,
                message: row[0].hiragana + ": " +row[0].mean,
                sound: false
            });
        }
    });
    connection.end(function () {
    });
    setTimeout(notification, 10000);
}
